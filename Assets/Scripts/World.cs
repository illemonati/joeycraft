using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {
    public Material material;
    public Material transparentMaterial;
    
    
    public Vector3 spawnLocation;
    public BlockType[] blockTypes;
    public Transform player;
    public int seed;
    public BiomeAttributes biomeAttributes;
    public GameObject DebugScreen;
    public ChunkCoord playerChunkCoord;

    private Chunk[,] chunks = new Chunk[VoxelData.WorldSizeInChunks, VoxelData.WorldSizeInChunks];
    private List<ChunkCoord> activeChunks = new List<ChunkCoord>();
    private ChunkCoord playerLastChunkCoord;

    private List<ChunkCoord> chunksToCreate = new List<ChunkCoord>();
    private List<Chunk> chunksToUpdate = new List<Chunk>();
    private bool isCreatingChunks = false;
    
    private Queue<VoxelMod> modifications = new Queue<VoxelMod>();
    
    

    void Start() {
        
        Random.InitState(seed);
        spawnLocation = new Vector3((VoxelData.WorldSizeInChunks * VoxelData.ChunkWidth) / 2f,
            VoxelData.ChunkHeight - 50f, (VoxelData.WorldSizeInChunks * VoxelData.ChunkWidth) / 2f);
        GenerateWorld();
        playerLastChunkCoord = GetChunkCoordFromVec3(player.position);
    }

    void Update() {
        playerChunkCoord = GetChunkCoordFromVec3(player.position);
        if (!playerChunkCoord.Equals(playerLastChunkCoord)) {
            CheckViewDistance();
            playerLastChunkCoord = playerChunkCoord;
        }

        if (chunksToCreate.Count > 0 && !isCreatingChunks) StartCoroutine(nameof(CreateChunks));

        if (Input.GetKeyDown(KeyCode.F3)) {
            DebugScreen.SetActive(!DebugScreen.activeSelf);
        }
        
    }

    void GenerateWorld() {
        
        player.position = spawnLocation;
        
        
        for (var x = Mathf.Max((int) (VoxelData.WorldSizeInChunks / 2f - VoxelData.ViewDistanceInChunks), 0);
            x < Mathf.Min((VoxelData.WorldSizeInChunks / 2f) + VoxelData.ViewDistanceInChunks, VoxelData.WorldSizeInChunks);
            x++) {
            for (var z =  Mathf.Max((int) (VoxelData.WorldSizeInChunks / 2f - VoxelData.ViewDistanceInChunks), 0);
                z <  Mathf.Min((VoxelData.WorldSizeInChunks / 2f) + VoxelData.ViewDistanceInChunks, VoxelData.WorldSizeInChunks);
                z++) {
                // Debug.Log($"{x}, {z}");
                chunks[x, z] = new Chunk(new ChunkCoord(x, z), this, true);
                activeChunks.Add(new ChunkCoord(x, z));
            }
        }

        CheckViewDistance();
        // Debug.Log("ViewDistanceDOne");
        CheckForModifications();
    }

    void CheckForModifications() {
        while (modifications.Count > 0) {
            // Debug.Log($"Modifications Count: {modifications.Count}");
            var vm = modifications.Dequeue();
            var c = GetChunkCoordFromVec3(vm.position);
            if (chunks[c.x, c.z] == null) {
                chunks[c.x, c.z] = new Chunk(c, this, true);
                activeChunks.Add(c);
            }
            
            chunks[c.x, c.z].modifications.Enqueue(vm);
        
            if (!chunksToUpdate.Contains(chunks[c.x, c.z])) {
                chunksToUpdate.Add(chunks[c.x, c.z]);
            }
        
            for (var i = 0; i < chunksToUpdate.Count; i++) {
                chunksToUpdate[0].UpdateChunk();
                chunksToUpdate.RemoveAt(0);
            }
        }
    }

    ChunkCoord GetChunkCoordFromVec3(Vector3 pos) {
        var x = Mathf.Floor(pos.x) / VoxelData.ChunkWidth;
        var z = Mathf.Floor(pos.z) / VoxelData.ChunkWidth;
        return new ChunkCoord((int) x, (int) z);
    }

    public Chunk GetChunkFromVec3(Vector3 pos) {
        var coord = GetChunkCoordFromVec3(pos);
        return chunks[coord.x, coord.z];
    }

    IEnumerator CreateChunks() {
        isCreatingChunks = true;

        while (chunksToCreate.Count > 0) {
            chunks[chunksToCreate[0].x, chunksToCreate[0].z].Initialize();
            chunksToCreate.RemoveAt(0);
            yield return null;
        }

        isCreatingChunks = false;
    }

    void CheckViewDistance() {
        var previouslyActiveChunks = new List<ChunkCoord>(activeChunks);

        var position = player.position;
        var chunkX = Mathf.FloorToInt(position.x / VoxelData.ChunkWidth);
        var chunkZ = Mathf.FloorToInt(position.z / VoxelData.ChunkWidth);
        for (var x = chunkX - VoxelData.ViewDistanceInChunks / 2;
            x < chunkX + VoxelData.ViewDistanceInChunks / 2;
            x++) {
            for (var z = chunkZ - VoxelData.ViewDistanceInChunks / 2;
                z < chunkZ + VoxelData.ViewDistanceInChunks / 2;
                z++) {
                if (!isChunkInWorld(new ChunkCoord(x, z))) continue;
                if (chunks[x, z] == null) {
                    chunks[x, z] = new Chunk(new ChunkCoord(x, z), this, false);
                    chunksToCreate.Add(new ChunkCoord(x, z));
                }

                else if (!chunks[x, z].isActive) {
                    chunks[x, z].isActive = true;
                }

                activeChunks.Add(new ChunkCoord(x, z));

                for (var i = 0; i < previouslyActiveChunks.Count; i++) {
                    if (previouslyActiveChunks[i].Equals(new ChunkCoord(x, z))) {
                        previouslyActiveChunks.RemoveAt(i);
                    }
                }
            }
        }

        foreach (var coord in previouslyActiveChunks) {
            chunks[coord.x, coord.z].isActive = false;
            activeChunks.Remove(coord);
        }
    }


    public bool CheckForVoxel(Vector3 position) {
        var thisChunk = new ChunkCoord(position);
        if (!IsVoxelInWorld(position)) return false;

        var chunk = chunks[thisChunk.x, thisChunk.z];
        if (chunk != null && chunk.isVoxelMapPopulated) {
            return blockTypes[chunk.GetVoxelFromGlobalVector3(position)].isSolid;
        }

        return blockTypes[GetVoxel(position)].isSolid;
    }
    
    public bool CheckIfVoxelTransparent(Vector3 position) {
        var thisChunk = new ChunkCoord(position);
        if (!IsVoxelInWorld(position)) return false;

        var chunk = chunks[thisChunk.x, thisChunk.z];
        if (chunk != null && chunk.isVoxelMapPopulated) {
            return blockTypes[chunk.GetVoxelFromGlobalVector3(position)].isTransparent;
        }

        return blockTypes[GetVoxel(position)].isTransparent;
    }

    public byte GetVoxel(Vector3 position) {
        var yPos = Mathf.FloorToInt(position.y);

        /* IMMUTABLE PASS */

        // If outside world, return air
        if (!IsVoxelInWorld(position)) return 0;

        // If bottom block of chunk, return bedrock
        if (yPos < 1) return 1;

        /* BASIC TERRAIN PASS */

        var terrainHeight = Mathf.FloorToInt(
            Noise.Get2DPerlin(
                new Vector2(position.x, position.z),
                0,
                biomeAttributes.terrainScale
            ) * biomeAttributes.terrainHeight
        ) + biomeAttributes.solidGroundHeight;
        var voxelVal = (byte) 0;

        if (yPos == terrainHeight) voxelVal = 3;
        else if (yPos < terrainHeight && yPos > terrainHeight - 4) voxelVal = 5;
        else if (yPos > terrainHeight) return 0;
        else voxelVal = 2;

        /* 2nd PASS */
        if (voxelVal == 2) {
            foreach (var lode in biomeAttributes.lodes) {
                if (yPos < lode.minHeight || yPos > lode.maxHeight) continue;
                if (Noise.Get3DPerlin(position, lode.noiseOffset, lode.scale, lode.threshold)) {
                    voxelVal = lode.blockId;
                }
            }
        }
        
        /* Tree PASS */
        if (yPos == terrainHeight) {
            if (Noise.Get2DPerlin(new Vector2(position.x, position.z), 24, biomeAttributes.treeZoneScale) >
                biomeAttributes.treeZoneThreshold) {
                voxelVal = 1;
                if (Noise.Get2DPerlin(new Vector2(position.x, position.z), 12, biomeAttributes.treePlacementScale) >
                    biomeAttributes.treePlacementThreshold) {

                    Structure.MakeTree(position, modifications, biomeAttributes.minTreeHeight, biomeAttributes.maxTreeHeight);
                    // Debug.Log("MakeTreeCalled");
                    // voxelVal = 7;
                }
            }
        }

        return voxelVal;
    }


    // void CreateNewChunk(int x, int z) {
    //     chunks[x, z] = new Chunk(new ChunkCoord(x, z), this);
    //     activeChunks.Add(new ChunkCoord(x, z));
    // }

    bool isChunkInWorld(ChunkCoord coord) => (
        coord.x >= 0 && coord.x < VoxelData.WorldSizeInChunks &&
        coord.z >= 0 && coord.z < VoxelData.WorldSizeInChunks);

    bool IsVoxelInWorld(Vector3 coord) => (
        coord.x >= 0 && coord.x < VoxelData.WorldSizeInVoxels &&
        coord.y >= 0 && coord.y < VoxelData.ChunkHeight &&
        coord.z >= 0 && coord.z < VoxelData.WorldSizeInVoxels
    );
}


[System.Serializable]
public class BlockType {
    public string blockName;
    public bool isSolid;
    public bool isTransparent;
    public Sprite icon;

    [Header("Texture Values")] public int backFaceTexture;
    public int frontFaceTexture;
    public int topFaceTexture;
    public int bottomFaceTexture;
    public int leftFaceTexture;
    public int rightFaceTexture;


    // Back, Front, Top, Bottom, Left, Right
    public int GetTextureId(int faceIndex) {
        var faceTextures = new int[] {
            backFaceTexture,
            frontFaceTexture,
            topFaceTexture,
            bottomFaceTexture,
            leftFaceTexture,
            rightFaceTexture
        };
        return faceTextures[faceIndex];
    }
}


[System.Serializable]
public class VoxelMod {
    public Vector3 position;
    public byte id;

    public VoxelMod() : this(new Vector3(0, 0, 0), 0) { }


    public VoxelMod(Vector3 position, byte id) {
        this.position = position;
        this.id = id;
    }
}

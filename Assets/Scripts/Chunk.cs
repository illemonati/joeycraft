using System;
using System.Collections.Generic;
using UnityEngine;

public class Chunk {
    // Start is called before the first frame update
    public ChunkCoord coord;

    GameObject chunkObject;
    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    World world;

    private bool _isActive = false;
    private int vertexIndex = 0;
    private List<Vector3> vertices = new List<Vector3>();
    private List<int> triangles = new List<int>();
    private List<Vector2> uvs = new List<Vector2>();
    private List<int> transparentTriangles = new List<int>();
    private Material[] materials = new Material[2];

    public byte[,,] VoxelMap = new byte[VoxelData.ChunkWidth, VoxelData.ChunkHeight, VoxelData.ChunkWidth];
    public bool isVoxelMapPopulated = false;
    
    public Queue<VoxelMod> modifications = new Queue<VoxelMod>();

    public Chunk(ChunkCoord coord, World world, bool generateOnLoad) {
        this.world = world;
        this.coord = coord;
        this.isActive = true;

        if (generateOnLoad) Initialize();
    }

    public void Initialize() {
        this.chunkObject = new GameObject();
        this.meshFilter = chunkObject.AddComponent<MeshFilter>();
        this.meshRenderer = chunkObject.AddComponent<MeshRenderer>();

        materials[0] = world.material;
        materials[1] = world.transparentMaterial;
        meshRenderer.materials = materials;
        
        chunkObject.transform.SetParent(this.world.transform);
        chunkObject.transform.position = new Vector3(
            coord.x * VoxelData.ChunkWidth,
            0,
            coord.z * VoxelData.ChunkWidth
        );
        chunkObject.name = $"Chunk {coord.x}, {coord.z}";

        PopulateVoxelMap();
        UpdateChunk();
    }

    public void UpdateChunk() {
        // Debug.Log($"Update chunk, modifications: {modifications.Count}");
        while (modifications.Count > 0) {
            var vm = modifications.Dequeue();
            // Debug.Log(vm);
            var pos = vm.position -= position;
            try {
                VoxelMap[(int) pos.x, (int) pos.y, (int) pos.z] = vm.id;
            }
            catch (IndexOutOfRangeException e) {
                
                
            }
        }
        
        ClearMeshData();
        for (var x = 0; x < VoxelData.ChunkWidth; x++) {
            for (var y = 0; y < VoxelData.ChunkHeight; y++) {
                for (var z = 0; z < VoxelData.ChunkWidth; z++) {
                    if (!world.blockTypes[VoxelMap[x, y, z]].isSolid) continue;
                    UpdateMeshData(new Vector3(x, y, z));
                }
            }
        }

        CreateMesh();
    }

    bool IsVoxelInChunk(int x, int y, int z) =>
        !(
            x < 0 || x > VoxelData.ChunkWidth - 1 ||
            y < 0 || y > VoxelData.ChunkHeight - 1 ||
            z < 0 || z > VoxelData.ChunkWidth - 1
        );


    public bool isActive {
        get => chunkObject.activeSelf;
        set {
            _isActive = value;
            if (chunkObject != null) {
                chunkObject.SetActive(value);
            }
        }
    }

    public Vector3 position => chunkObject.transform.position;


    public void EditVoxel(Vector3 pos, byte newID) {
        // Debug.Log(1);
        var xCheck = Mathf.FloorToInt(pos.x);
        var zCheck = Mathf.FloorToInt(pos.z);
        var yCheck = Mathf.FloorToInt(pos.y);

        var position1 = chunkObject.transform.position;
        xCheck -= Mathf.FloorToInt(position1.x);
        zCheck -= Mathf.FloorToInt(position1.z);

        VoxelMap[xCheck, yCheck, zCheck] = newID;
        
        UpdateSurroundingVoxels(xCheck, yCheck, zCheck);
        UpdateChunk();
    }

    public void UpdateSurroundingVoxels(int x, int y, int z) {
        Vector3 thisVoxel = new Vector3(x, y, z);
        for (var p = 0; p < 6; p++) {
            var currentVoxel = thisVoxel + VoxelData.FaceChecks[p];

            if (!IsVoxelInChunk((int) currentVoxel.x, (int) currentVoxel.y, (int) currentVoxel.z)) {
                world.GetChunkFromVec3(currentVoxel + position).UpdateChunk();
            }
        }
    }

    public byte GetVoxelFromGlobalVector3(Vector3 position) {
        var xCheck = Mathf.FloorToInt(position.x);
        var zCheck = Mathf.FloorToInt(position.z);
        var yCheck = Mathf.FloorToInt(position.y);

        var position1 = chunkObject.transform.position;
        xCheck -= Mathf.FloorToInt(position1.x);
        zCheck -= Mathf.FloorToInt(position1.z);

        return VoxelMap[xCheck, yCheck, zCheck];
    }

    bool CheckVoxel(Vector3 pos) {
        var x = Mathf.FloorToInt(pos.x);
        var y = Mathf.FloorToInt(pos.y);
        var z = Mathf.FloorToInt(pos.z);
        return !IsVoxelInChunk(x, y, z)
            ? world.CheckIfVoxelTransparent(pos + position)
            : world.blockTypes[VoxelMap[x, y, z]].isTransparent;
    }

    void PopulateVoxelMap() {
        for (var x = 0; x < VoxelData.ChunkWidth; x++) {
            for (var y = 0; y < VoxelData.ChunkHeight; y++) {
                for (var z = 0; z < VoxelData.ChunkWidth; z++) {
                    VoxelMap[x, y, z] = world.GetVoxel(new Vector3(x, y, z) + position);
                }
            }
        }

        isVoxelMapPopulated = true;
    }

    void UpdateMeshData(Vector3 position) {
        var blockId = VoxelMap[(int) position.x, (int) position.y, (int) position.z];
        var isTransparent = world.blockTypes[blockId].isTransparent;
        
        
        for (var p = 0; p < 6; p++) {
            if (!CheckVoxel(position + VoxelData.FaceChecks[p])) {
                continue;
            }

            

            for (var i = 0; i < 4; i++) {
                vertices.Add(position + VoxelData.VoxelVertices[VoxelData.VoxelTriangles[p, i]]);
            }

            AddTexture(world.blockTypes[blockId].GetTextureId(p));
            if (!isTransparent) {
                triangles.Add(vertexIndex);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 3);
            }

            else {
                transparentTriangles.Add(vertexIndex);
                transparentTriangles.Add(vertexIndex + 1);
                transparentTriangles.Add(vertexIndex + 2);
                transparentTriangles.Add(vertexIndex + 2);
                transparentTriangles.Add(vertexIndex + 1);
                transparentTriangles.Add(vertexIndex + 3);
            }
            vertexIndex += 4;
        }
    }

    void CreateMesh() {
        var mesh = new Mesh {
            vertices = vertices.ToArray(),
            subMeshCount = 2,
            uv = uvs.ToArray()
        };
        
        mesh.SetTriangles(triangles.ToArray(), 0);
        mesh.SetTriangles(transparentTriangles.ToArray(), 1);
        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;
    }

    void ClearMeshData() {
        vertexIndex = 0;
        vertices.Clear();
        triangles.Clear();
        transparentTriangles.Clear();
        uvs.Clear();
    }

    void AddTexture(int textureId) {
        var y = (float) (textureId / VoxelData.TextureAlasSizeInBlocks);
        var x = textureId - (y * VoxelData.TextureAlasSizeInBlocks);

        x *= VoxelData.NormalizedBlockTextureSize;
        y *= VoxelData.NormalizedBlockTextureSize;
        y = 1f - y - VoxelData.NormalizedBlockTextureSize;
        foreach (var uv in VoxelData.VoxelUvs) {
            uvs.Add(uv * VoxelData.NormalizedBlockTextureSize + new Vector2(x, y));
        }
    }

    // Update is called once per frame
    void Update() { }
}

public class ChunkCoord {
    public int x;
    public int z;

    public ChunkCoord() {
        x = 0;
        z = 0;
    }

    public ChunkCoord(Vector3 position) {
        var xCheck = Mathf.FloorToInt(position.x);
        var zCheck = Mathf.FloorToInt(position.z);

        x = xCheck / VoxelData.ChunkWidth;
        z = zCheck / VoxelData.ChunkWidth;
    }

    public ChunkCoord(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public override bool Equals(object other) {
        if (other == null || this.GetType().IsInstanceOfType(other.GetType())) {
            return false;
        }

        var otherCoord = (ChunkCoord) other;
        return (x == otherCoord.x) && (z == otherCoord.z);
    }
}
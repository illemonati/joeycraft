using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toolbar : MonoBehaviour {
    public World world;
    public Player player;

    public RectTransform highlight;
    public ItemSlot[] itemSlots;

    private int slotIndex = 0;
    
    private int previousSlotIndex = 0;

    private KeyCode[] keyCodes = {
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9,
        KeyCode.Alpha0,
    };

    private void Start() {
        world = GameObject.Find("World").GetComponent<World>();
        foreach (var slot in itemSlots) {
            slot.icon.sprite = world.blockTypes[slot.itemId].icon;
            slot.icon.enabled = true;
        }
        highlight.position = itemSlots[slotIndex].icon.transform.position;
        player.selectedBlockIndex = itemSlots[slotIndex].itemId;
    }

    private void Update() {
        var scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll > 0) {
            slotIndex--;
        }
        else if (scroll < 0) {
            slotIndex++;
        }

        for (var i = 0; i < keyCodes.Length; i++) {
            if (!Input.GetKeyDown(keyCodes[i])) continue;
            slotIndex = i;
        }

        if (slotIndex == previousSlotIndex) return;
        if (slotIndex > itemSlots.Length - 1) {
            slotIndex = 0;
        }
        else if (slotIndex < 0) {
            slotIndex = itemSlots.Length - 1;
        }

        highlight.position = itemSlots[slotIndex].icon.transform.position;
        player.selectedBlockIndex = itemSlots[slotIndex].itemId;
        previousSlotIndex = slotIndex;
    }
}


[System.Serializable]
    public class ItemSlot {
        public byte itemId;
        public Image icon;
    }
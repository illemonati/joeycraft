using UnityEngine;
using UnityEngine.UIElements;

public static class Noise {
    public static float Get2DPerlin(Vector2 position, float offset, float scale) {
        return Mathf.PerlinNoise(
            (position.x + 0.1f + offset) / VoxelData.ChunkWidth * scale,
            (position.y + 0.1f + offset) / VoxelData.ChunkWidth * scale
        );
    }

    public static bool Get3DPerlin(Vector3 position, float offset, float scale, float threshold) {
        var x = (position.x + 0.1f + offset) * scale;
        var y = (position.y + 0.1f + offset) * scale;
        var z = (position.z + 0.1f + offset) * scale;
        var AB = Mathf.PerlinNoise(x, y);
        var BC = Mathf.PerlinNoise(y, z);
        var AC = Mathf.PerlinNoise(x, z);
        var BA = Mathf.PerlinNoise(y, x);
        var CB = Mathf.PerlinNoise(z, y);
        var CA = Mathf.PerlinNoise(z, x);

        return (AB + BC + AC + BA + CB + CA) / 6f > threshold;
    }
}
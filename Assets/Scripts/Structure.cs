


using System.Collections.Generic;
using UnityEngine;

public static class Structure {
    public static void MakeTree(Vector3 position, Queue<VoxelMod> queue, int minTrunkHeight, int maxTrunkHeight) {
        var height = (int) (maxTrunkHeight * Noise.Get2DPerlin(new Vector2(position.x, position.z), 1230f, 3f));
        if (height < minTrunkHeight) height = minTrunkHeight;

        // Debug.Log($"Tree at {position.x} {position.y} {position.z}");

        for (var i = 1; i < height; i++) {
            queue.Enqueue(new VoxelMod(new Vector3(position.x, position.y + i, position.z), 6));
        }
        queue.Enqueue(new VoxelMod(new Vector3(position.x, position.y + height, position.z), 11));
        
        // for (var x = -3; x < 4; x++){
        //     for (var y = 0; y < 7; y++) {
        //         for (var z = -3; z < 4; z++) {
        //             queue.Enqueue(new VoxelMod(new Vector3(position.x + x, position.y + height + y, position.z + z), 11));
        //         }
        //     }
        // }
    }
}

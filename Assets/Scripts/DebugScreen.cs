using UnityEngine;
using UnityEngine.UI;

public class DebugScreen : MonoBehaviour {
    public World world;
    public Text textComponent;

    private float frameRate;
    private float timer;
    private void Start() {
        world = GameObject.Find("World").GetComponent<World>();
        textComponent = GetComponent<Text>();
    }

    private void Update() {
        var position = world.player.transform.position;
        var x = Mathf.FloorToInt(position.x);
        var y = Mathf.FloorToInt(position.y);
        var z = Mathf.FloorToInt(position.z);
        
        var debugText = "JoeyCraft v0.0.1-beta.20210131";
        debugText += "\n";
        
        debugText += $"{frameRate} fps";
        debugText += "\n\n";

        debugText += $"XYZ: {x} {y} {z}";
        debugText += "\n";

        debugText += $"chunkXZ: {world.playerChunkCoord.x} {world.playerChunkCoord.z}";
        
        textComponent.text = debugText;

        if (timer > 1f) {
            frameRate = Mathf.FloorToInt(1 / Time.unscaledDeltaTime);
            timer = 0;
        }
        else {
            timer += Time.unscaledDeltaTime;
        }
    }
}
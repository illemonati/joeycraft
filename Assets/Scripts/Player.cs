using System;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    public bool isGrounded;
    public bool isSprinting = false;

    private Transform camera;
    private World world;

    public Transform highlightBlock;
    public Transform placeHighlightBlock;
    public byte selectedBlockIndex = 1;
    public float checkIncrement = 0.1f;
    public float reach = 8;
    
    public float sprintSpeed = 6f;
    public float jumpForce = 5f;
    public float walkSpeed = 3f;
    public float gravity = -9.8f;

    public float playerWidth = 0.3f;

    private float horizontal;
    private float vertical;
    private float mouseHorizontal;
    private float mouseVertical;
    private Vector3 velocity;
    private float verticalMomentum;
    private bool jumpRequest;

    private void Start() {
        camera = GameObject.FindWithTag("MainCamera").transform;
        world = GameObject.Find("World").GetComponent<World>();

        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Awake () {
#if UNITY_EDITOR
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;
#endif
    }

    private void Update() {
        GetPlayerInputs();
        PlaceCursorBlocks();
    }

    private void FixedUpdate() {
        CalculateVelocity();
        if (jumpRequest) {
            Jump();
        }
        transform.Rotate(Vector3.up * mouseHorizontal);
        camera.Rotate(Vector3.right * -mouseVertical);
        transform.Translate(velocity, Space.World);
    }

    private void Jump() {
        verticalMomentum = jumpForce;
        isGrounded = false;
        jumpRequest = false;
    }

    private void CalculateVelocity() {
        // velocity = (transform.forward * vertical + transform.right * horizontal) * (Time.deltaTime * walkSpeed);
        // velocity += Vector3.up * (gravity * Time.deltaTime);
        // velocity.y = CheckDownSpeed(velocity.y);

        // Affect vertical momentum with gravity
        if (verticalMomentum > gravity) {
            verticalMomentum += Time.fixedDeltaTime * gravity;
        }

        // Forward
        var transform1 = transform;
        velocity = (transform1.forward * vertical + transform1.right * horizontal) *
                   (Time.fixedDeltaTime * (isSprinting ? sprintSpeed : walkSpeed));
        
        // Apply vertical momentum
        velocity += Vector3.up * (verticalMomentum * Time.fixedDeltaTime);

        if (velocity.z > 0 && front || velocity.z < 0 && back) {
            velocity.z = 0;
        }
        if (velocity.x > 0 && right || velocity.x < 0 && left) {
            velocity.x = 0;
        }

        if (velocity.y < 0) {
            velocity.y = CheckDownSpeed(velocity.y);
        } else if (velocity.y > 0) {
            velocity.y = checkUpSpeed(velocity.y);
        }
    }

    private void GetPlayerInputs() {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        mouseHorizontal = Input.GetAxis("Mouse X");
        mouseVertical = Input.GetAxis("Mouse Y");
        if (Input.GetButtonDown("Sprint")) isSprinting = true;
        if (Input.GetButtonUp("Sprint")) isSprinting = false;

        if (isGrounded && Input.GetButtonDown("Jump")) jumpRequest = true;

        if (highlightBlock.gameObject.activeSelf) {
            if (Input.GetMouseButtonDown(0)) {
                var position = highlightBlock.position;
                world.GetChunkFromVec3(position).EditVoxel(
                    position,
                    0
                );
            }
            if (Input.GetMouseButtonDown(1)) {
                var position = placeHighlightBlock.position;
                world.GetChunkFromVec3(position).EditVoxel(
                    position,
                    selectedBlockIndex
                );
            }
        }
    }

    public void PlaceCursorBlocks() {
        var step = checkIncrement;
        var lastPos = new Vector3();

        // Raycast kind of 
        while (step < reach) {
            var pos = camera.position + (camera.forward * step);
            if (world.CheckForVoxel(pos)) {
                highlightBlock.position = new Vector3(
                    Mathf.FloorToInt(pos.x),
                    Mathf.FloorToInt(pos.y),
                    Mathf.FloorToInt(pos.z)
                );
                placeHighlightBlock.position = lastPos;
                highlightBlock.gameObject.SetActive(true);
                placeHighlightBlock.gameObject.SetActive(true);

                return;
            }

            lastPos = new Vector3(
                Mathf.FloorToInt(pos.x),
                Mathf.FloorToInt(pos.y),
                Mathf.FloorToInt(pos.z)
            );;
            step += checkIncrement;
        }
        
        highlightBlock.gameObject.SetActive(false);
        placeHighlightBlock.gameObject.SetActive(false);
        
    }

    private float CheckDownSpeed(float downSpeed) {
        if (
            world.CheckForVoxel(new Vector3(transform.position.x - playerWidth, transform.position.y + downSpeed,
                transform.position.z - playerWidth)) ||
            world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y + downSpeed,
                transform.position.z - playerWidth)) ||
            world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y + downSpeed,
                transform.position.z + playerWidth)) ||
            world.CheckForVoxel(new Vector3(transform.position.x - playerWidth, transform.position.y + downSpeed,
                transform.position.z + playerWidth))
        ) {
            // Debug.Log(1);
            isGrounded = true;
            return 0;
        }
        else {
            // Debug.Log(2);
            isGrounded = false;
            return downSpeed;
        }
    }

    private float checkUpSpeed(float upSpeed) {
        if (
            world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y + 2f + upSpeed,
                transform.position.z - playerWidth)) ||
            world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y + 2f + upSpeed,
                transform.position.z + playerWidth)) ||
            world.CheckForVoxel(new Vector3(transform.position.x - playerWidth, transform.position.y + 2f + upSpeed,
                transform.position.z + playerWidth))
        ) {
            return 0;
        }

        return upSpeed;
    }


    public bool front => (
        world.CheckForVoxel(new Vector3(transform.position.x, transform.position.y, transform.position.z + playerWidth)) ||
        world.CheckForVoxel(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z + playerWidth))
    );

    public bool back => (
        world.CheckForVoxel(new Vector3(transform.position.x, transform.position.y, transform.position.z - playerWidth)) ||
        world.CheckForVoxel(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z - playerWidth))
    );

    public bool left => (
        world.CheckForVoxel(new Vector3(transform.position.x - playerWidth, transform.position.y, transform.position.z)) ||
        world.CheckForVoxel(new Vector3(transform.position.x - playerWidth, transform.position.y + 1, transform.position.z))
    );

    public bool right => (
        world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y,
            transform.position.z + playerWidth)) ||
        world.CheckForVoxel(new Vector3(transform.position.x + playerWidth, transform.position.y + 1,
            transform.position.z + playerWidth))
    );
}